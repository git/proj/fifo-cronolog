CFLAGS += -Wall
BIN = fifo-cronolog
HELPER = fifo-cronolog-setup
SRC = $(BIN).c
OBJ = $(patsubst %.c,%.o,$(SRC))

BINDIR = /usr/sbin/
SYSTEMD_TARGET = /usr/lib/systemd/system/
SYSTEMD_UNIT = systemd/fifo-cronolog@.service

VERSION = $(shell awk -F'"' '/#define VERSION/{print $$2}' $(SRC) )
DISTFILE_EXT = tar.gz
DISTFILE = $(BIN)-$(VERSION).$(DISTFILE_EXT)

dist:
	if test -e $(DISTFILE) ; then echo "$(DISTFILE) already exists" ; exit 1 ; fi
	git archive --format $(DISTFILE_EXT) --prefix $(BIN)-$(VERSION)/ v$(VERSION) >$(DISTFILE).tmp && mv $(DISTFILE).tmp $(DISTFILE)

all: $(BIN)

$(BIN): $(OBJ)
	$(CC) $(LDFLAGS) -o $@ $<

.c.o:
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	$(RM) $(BIN) $(OBJ)

install:
	mkdir -p $(DESTDIR)$(BINDIR) $(DESTDIR)$(SYSTEMD_TARGET)
	install -m0755 -D $(BIN) $(HELPER) $(DESTDIR)$(BINDIR)
	install -m0644 -D $(SYSTEMD_UNIT) $(DESTDIR)$(SYSTEMD_TARGET)

#  vim: set ts=4 sw=4 tw=80:
