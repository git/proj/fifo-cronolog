1. Put env into `/etc/systemd-env/`, using the entire name of the unit, with instance suffix, and an extra `.env`,
   e.g. `/etc/systemd-env/fifo-cronolog@example.service.env`
2. `systemctl enable fifo-cronolog@example`
3. Use systemd overrides if you need to add dependencies.
