fifo-cronolog
-------------
This is a little tool that connects a named pipe/FIFO into the stdin of `cronolog`.

This is useful for cases where the logging destination cannot be specified as a
program. It was originally written to do logging from Squid, but later re-used
for Nginx & other tools.

Cronolog's canonical example, in an Apache configuration
```
TransferLog "|/www/sbin/cronolog /www/logs/%Y/%m/%d/access.log"
```

Is converted into:
1. Fifo-cronolog as (via your init system, so it gets restarted):
```
fifo-cronolog /run/apache-access.pid /run/apache-access.fifo /www/logs/%Y/%m/%d/access.log
```
2. Apache configuration:
```
TransferLog "/run/apache-access.fifo"
```
